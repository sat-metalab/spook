"""A basic OSC Server which catches and prints all data received from the
`skeletons` filter i.e. a list of keypoints for each pose detected.
"""

import re

import liblo

server = liblo.Server(9000)

# /livepose/{filter_name}/start_filter
START_FILTER_REGEX = r"/livepose/[a-zA-Z]+/start_filter"
# /livepose/{filter_name}/end_filter
END_FILTER_REGEX = r"/livepose/[a-zA-Z]+/end_filter"

# Skeletons Filter Paths
# /livepose/skeletons/{camera_id}/{pose_index}/start_pose
START_POSE_REGEX = r"/livepose/skeletons/[0-9]+/[0-9]+/start_pose"
# /livepose/skeletons/{camera_id}/{pose_index}/end_pose
END_POSE_REGEX = r"/livepose/skeletons/[0-9]+/[0-9]+/end_pose"
# /livepose/skeletons/{camera_id}/{pose_index}/pose_id
POSE_ID_REGEX = r"/livepose/skeletons/[0-9]+/[0-9]+/pose_id"
# /livepose/skeletons/{camera_id}/{pose_index}/keypoints/{keypoint_name}
KEYPOINTS_REGEX = r"/livepose/skeletons/[0-9]+/[0-9]+/keypoints/[a-zA-Z]+"


def frame_handler():
    print("frame started")

def end_frame_handler():
    print("frame ended")

def start_filter_handler(path):
    filter_name = path.split("/")[2]
    print(f"{filter_name} filter started.")

def end_filter_handler(path):
    filter_name = path.split("/")[2]
    print(f"{filter_name} filter ended.")

def start_pose_handler(path):
    print("pose started.")

def end_pose_handler(path):
    print("pose ended.")

def pose_id_handler(path, args):
    print(f"Pose ID: {args}")

def keypoints_handler(path, args):
    keypoint_name = path.split("/")[-1]
    print(f"{keypoint_name}: {args}")

def fallback_handler(path, args, types, src, data):
    """Pyliblo does not allow wildcards in OSC paths, so we need to use a
    generic handler to catch all other messages, and parse out the message
    paths manually inside of this handler.
    """
    if re.match(START_FILTER_REGEX, path) is not None:
        return start_filter_handler(path)

    elif re.match(END_FILTER_REGEX, path) is not None:
        return end_filter_handler(path)

    elif re.match(START_POSE_REGEX, path) is not None:
        return start_pose_handler(path)

    elif re.match(END_POSE_REGEX, path) is not None:
        return end_pose_handler(path)

    elif re.match(POSE_ID_REGEX, path) is not None:
        return pose_id_handler(path, args)

    elif re.match(KEYPOINTS_REGEX, path) is not None:
        return keypoints_handler(path, args)

    else:
        print(f"Message path '{path}' not implemented.")


server.add_method("/livepose/frame", None, frame_handler)
server.add_method("/livepose/end_frame", None, end_frame_handler)
server.add_method(None, None, fallback_handler)

while True:
    server.recv(100)
