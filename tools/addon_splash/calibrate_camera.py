"""
 This script is a first prototype. There are still limitations that need to be adressed:
   - the 3D calibration points are manually provided via the config file.
   - these 3D points are the same for all the cameras - > must be visible by all the cameras...


 The calibration process is based on OpenCV library.
 Non-planar calibration surfaces -> requires intrinsic parameters
 Planar surfaces -> No need to specify intrinsic parameters
                 -> The z-coordinate of the 3D points must be zero
"""

import cv2
import logging
import math
import numpy as np
import os
import splash
import sys

from typing import Any, Dict, List, Optional, Tuple

from config import Config
from splash_calibrate import SplashCalibrate

default_config: str = os.path.realpath(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "config/example.json"
    ))
init_successful: bool = False
logger = logging.getLogger(__name__)
splashCalibrator: Optional[SplashCalibrate] = None


def parse_args() -> Dict[str, Any]:
    # if the script receives arguments, it is stored in sys.argv
    args = {}
    if len(sys.argv) > 1:  # the first element of the list is always the script path
        # we have arguments to parse
        args["config"] = sys.argv[1]  # path to the camera calibration config file
    return args


def splash_init() -> None:
    global splashCalibrator
    global init_successful

    def mouse_xy(event: int, x: int, y: int, flag: int, params: List[Tuple[int, int]]) -> None:
        if event == cv2.EVENT_LBUTTONDBLCLK:
            params.append((x, y))

    # First, change the loopRate of the python script
    splash.set_object_attribute("_pythonArgScript", "loopRate", 10)

    args = parse_args()

    # create a config object holding the configuration parameters
    try:
        config = Config(path=args["config"] if "config" in args else default_config)
    except Exception as e:
        logger.error(f"Error while reading configuration file. Please verify the parameters: {e}")

    try:
        splashCalibrator = SplashCalibrate(planar=config.planar, keypoints_3d=config.calib_pts, cams_info=config.cams_info)
    except Exception as e:
        logger.error(f"Error during creation of the calibrator: {e}")
        return

    # Here we loop over the provided camera paths, open the camera and select the 2D points
    for camera in splashCalibrator._cam_dict:
        current_cam = splashCalibrator._cam_dict[camera]
        if not current_cam.open():
            break  # camera could not be open
        splashCalibrator.init_camera(camera)
        logger.warning("Double-click to select points on the image corresponding to the 3D points from the configuration file")
        while True:
            cv2.waitKey(10)
            splashCalibrator.update(camera)

            cv2.setMouseCallback("Calibrate " + camera, mouse_xy, splashCalibrator._2d_keypoints_dict[camera])
            if len(splashCalibrator._2d_keypoints_dict[camera]) == len(splashCalibrator._keypoints_3d):
                cv2.imshow("Calibrate " + camera, current_cam.frame)  # to show the last point of the list
                cv2.waitKey(200)
                cv2.destroyAllWindows()
                break
        current_cam.close()
    init_successful = True


def splash_loop() -> bool:
    global splashCalibrator
    global init_successful

    if not init_successful:
        return False
    if splashCalibrator is None:
        return False

    if not splashCalibrator._calibrated:
        splashCalibrator._calibrated = True
        for cam in splashCalibrator._cam_dict:
            splashCalibrator._proj_error = math.inf
            current_cam = splashCalibrator._cam_dict[cam]

            assert len(splashCalibrator._keypoints_3d) == len(
                splashCalibrator._2d_keypoints_dict[cam]), f"Error on {cam}: Same number of points are required to pair 2D points with 3D points"
            assert(current_cam.frame is not None)

            keypoints_2d = np.array(splashCalibrator._2d_keypoints_dict[cam], dtype=np.float32)
            keypoints_3d = np.array(splashCalibrator._keypoints_3d, dtype=np.float32)
            keypoints_2d.reshape((-1, 2))
            keypoints_3d.reshape((-1, 3))
            image_size = (current_cam.frame.shape[0], current_cam.frame.shape[1])

            # PLANAR CASE
            if splashCalibrator._planar:
                splashCalibrator._proj_error, current_cam._kmat, current_cam._dist_coeffs, Rvec, tvec = cv2.calibrateCamera(
                    objectPoints=np.array([keypoints_3d]),
                    imagePoints=np.array([keypoints_2d]),
                    cameraMatrix=current_cam.kmat,
                    imageSize=image_size,
                    distCoeffs=current_cam.dist_coeffs,
                    flags=cv2.CALIB_ZERO_TANGENT_DIST
                )
                Rvec = Rvec[0]
                tvec = tvec[0]
                (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
                current_cam._extrinsics = np.c_[rotation_mat, tvec]
            # For the non-planar case, an intrinsic matrix is necessary. We are only estimating the
            # extrinsic parameters
            elif np.any(current_cam._kmat) and not splashCalibrator._planar:  # NON-PLANAR
                (success, Rvec, tvec) = cv2.solvePnP(
                    objectPoints=np.array([keypoints_3d]),
                    imagePoints=np.array([keypoints_2d]),
                    cameraMatrix=current_cam.kmat,
                    distCoeffs=current_cam.dist_coeffs,
                    flags=cv2.SOLVEPNP_ITERATIVE
                )
                if not success:
                    logger.error(f"Calibration not saved for camera: '{cam}' because the parameters found are not good enough")
                    return

                (rotation_mat, jacobian) = cv2.Rodrigues(Rvec)
                current_cam._extrinsics = np.c_[rotation_mat, tvec]
                # compute reprojection error
                reprojected_pts, jacobian = cv2.projectPoints(objectPoints=np.array(
                    [keypoints_3d]), rvec=Rvec, tvec=tvec, cameraMatrix=current_cam.kmat, distCoeffs=current_cam.dist_coeffs)
                splashCalibrator._proj_error = cv2.norm(np.swapaxes(reprojected_pts, 0, 1), np.array([keypoints_2d]))
            else:
                print(
                    f"Settings incomplete for {cam}. Planar case takes optional intrinsic parameters. For non-planar surfaces, intrinsics parameters are required")
                return

            if splashCalibrator._proj_error < 10.0:
                splashCalibrator.save2file(cam, f"{cam}_parameters.json")
                logger.info(f"Calibration parameters saved to file: {cam}_parameters.json")
            else:
                logger.error(
                    f"Calibration not saved for '{cam}' because the reprojection error was too high: {splashCalibrator._proj_error}")

    return True


def splash_stop() -> None:
    logger.info("Shutting down the camera calibration process")
