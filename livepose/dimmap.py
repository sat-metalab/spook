import argparse

from abc import ABCMeta, abstractmethod

from typing import Any, Callable, Dict, Optional, Type, Union

from livepose.dataflow import Flow
from livepose.node import Node


def register_dimmap(name: str) -> Callable:
    """
    Decorator for registering dimension mappings
    :param name: name - Name of the filter
    """

    def deco(cls: Type['DimMap']) -> Callable:
        DimMap.register(dimmap_type=cls, name=name)
        return cls

    return deco


class DimMap(Node):
    """
    Base class for classes meant to compute new dimension mappings
    """

    Result = Dict[Union[str, int], Any]

    registered_dimmaps: Dict[str, Type['DimMap']] = dict()

    def __init__(self, dimmap_name: str, *args: Any, **kwargs: Any):
        """Init DimMap class."""
        super().__init__(**kwargs)
        self._result: 'DimMap.Result' = {}
        self._dimmap_name: str = dimmap_name

    @abstractmethod
    def add_parameters(self) -> None:
        """Add DimMap parameters."""
        pass

    @abstractmethod
    def init(self) -> None:
        """Init DimMap objects."""
        pass

    @classmethod
    def register(cls, dimmap_type: Type['DimMap'], name: str) -> None:
        if name not in cls.registered_dimmaps:
            cls.registered_dimmaps[name] = dimmap_type
        else:
            raise Exception(f"An dimension mapping typed {name} has already been registered")

    @classmethod
    def create_from_name(cls, name: str, **kwargs: Any) -> Optional['DimMap']:
        if name not in DimMap.registered_dimmaps:
            return None
        else:
            return DimMap.registered_dimmaps[name](**kwargs)

    @property
    def dimmap_name(self) -> str:
        return self._dimmap_name

    @property
    def result(self) -> Result:
        return self._result

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Process the given flow
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        self._result.clear()
