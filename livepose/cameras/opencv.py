import sys
import subprocess
import cv2

from livepose.dataflow import Channel
from livepose.camera import register_camera, Camera
from typing import Any, Callable, Dict, List, Optional, Tuple, Type

import logging
import numpy as np

logger = logging.getLogger(__name__)


@register_camera("opencv")
class OpenCVCamera(Camera):
    """
    Utility class embedding a camera with opencv, its parameters and buffers
    """

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()

    def init(self) -> None:

        self._camera: cv2.VideoCapture = cv2.VideoCapture()
        self._backend = cv2.CAP_ANY

        if self._args.path.startswith("/dev/video"):
            self._backend = cv2.CAP_V4L2

        if self._camera.open(self._args.path, self._backend):
            logging.info(f"opencv path: {self._args.path}")
            logging.info(f"opencv backend: {self._camera.getBackendName()}")

        if self._args.path.startswith("/dev/video") and int(self._camera.get(cv2.CAP_PROP_BACKEND)) is cv2.CAP_V4L2:
            logger.info(f"v4l-ctrl: set exposure_auto_priority to 0")
            try:
                subprocess.check_output(["v4l2-ctl", "-d", self._args.path, "-c",
                                         "exposure_auto_priority=0"], stderr=subprocess.STDOUT)
            except FileNotFoundError:
                logger.error(f"Could not run v4l2-ctl, please install v4l-utils")
                sys.exit(1)
            except subprocess.CalledProcessError as e:
                output = e.output.decode()
                if output.startswith("unknown control"):
                    logger.warn(f"{output}")
                else:
                    logger.error(f"Could not set v4l2 exposure_auto_priority to 0")
                    sys.exit(1)

    @property
    def fps(self) -> float:
        """
        Get the grab framerate for the camera
        :return: float - Framerate as frames per second
        """
        return float(self._camera.get(cv2.CAP_PROP_FPS))

    @fps.setter
    def fps(self, framerate: float) -> None:
        """
        Set the grab framerate, in frames per seconde
        :param: float - Framerate
        """
        self._camera.set(cv2.CAP_PROP_FPS, framerate)

    @property
    def resolution(self) -> Tuple[int, int]:
        """
        Get the resolution of the device
        :return: Tuple[int, int]
        """
        width = int(self._camera.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(self._camera.get(cv2.CAP_PROP_FRAME_HEIGHT))
        return (width, height)

    @resolution.setter
    def resolution(self, res: Tuple[int, int]) -> None:
        """
        Set the resolution of the device
        :param: Tuple[int, int]
        """
        self._camera.set(cv2.CAP_PROP_FRAME_WIDTH, res[0])
        self._camera.set(cv2.CAP_PROP_FRAME_HEIGHT, res[1])

    def is_open(self) -> bool:
        """
        Check if a video capturing device has been initialized already
        :return: bool
        """
        return self._camera.isOpened()

    def grab(self) -> bool:
        """
        Grabs the next frame from video file or capturing device
        :return: bool - in the case of success
        """
        return self._camera.grab()

    def retrieve(self) -> bool:
        """
        Decodes and set the grabbed video frame
        grab() should be called beforehand
        :return: bool - true in the case of success
        """
        ret, frame = self._camera.retrieve()

        if not ret:
            self._frame = None
            return False

        self._frame = frame

        return True

    def get_raw_channels(self) -> List[Channel]:
        """
        Get the raw channels from the camera, without any
        transformation (flip, rotate, etc) applied
        :return: List[Channel] - List of raw channels
        """
        channels: List[Channel] = []

        if self._frame is not None:
            channels.append(Channel(
                type=Channel.Type.COLOR,
                name=self._args.path,
                data=self._frame,
                metadata={}
            ))

        return channels

    def open(self) -> bool:
        """
        Open video file or device
        :return: bool - true if successful
        """
        return self._camera.open(self._args.path, self._backend)

    def close(self) -> None:
        """
        Closes video file or capturing device
        """
        self._camera.release()

    @property  # type: ignore
    def rotate(self) -> int:
        """
        Get the status of rotation (clockwise)
        :return: int - rotation
        """
        return self._args.rotate

    @rotate.setter  # type: ignore
    def rotate(self, rotate: int) -> None:
        """
        Set camera rotation (clockwise)
        :param: int - rotation
        """
        self._args.rotate = rotate
