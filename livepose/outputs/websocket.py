import asyncio
import json
import logging
import os
import socket
import ssl
import threading
import time
from dataclasses import dataclass
from typing import Any, Dict, List, Optional

# NOTE: Ignore typing for these since mypy does not recognize the aliases
# used by websockets for imports.
# See websockets docs for details on their API / module structure.
import websockets as ws
from websockets import serve  # type: ignore
from websockets.exceptions import ConnectionClosed
from websockets.server import WebSocketServerProtocol  # type: ignore

from livepose.dataflow import Channel, Flow, Stream
from livepose.output import register_output, Output

logger = logging.getLogger(__name__)


def create_ssl_context(cert_path: str = "",
                       key_path: str = "") -> Optional[ssl.SSLContext]:
    """ Creates new SSL context.
    Loads a private key and its corresponding certificate.
    :param cert_path: str - Path to the ssl certificate.
    Path should be either absolute or relative to the working directory.
    :param key_path: str - Path to the ssl certificate.
    Path should be either absolute or relative to the working directory.
    directory, to the ssl key.
    :return: ssl.SSLContext - a new SSL context.
    """
    if not cert_path or not key_path:
        return None  # We're not using SSL. That's fine.
    root_path = os.path.realpath('.')
    if os.path.isabs(cert_path):
        cert_abs_path = cert_path
    else:
        cert_abs_path = os.path.join(root_path, cert_path)
    if os.path.isabs(key_path):
        key_abs_path = key_path
    else:
        key_abs_path = os.path.join(root_path, key_path)
    if not os.path.exists(cert_abs_path) or not os.path.exists(key_abs_path):
        raise FileNotFoundError("SSL key or certificate not found. "
                                "Paths should be either absolute or relative to the working folder.")
    ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    ssl_ctx.load_cert_chain(cert_abs_path, key_abs_path)
    return ssl_ctx


@dataclass
class ClientConnection:
    """
    Websocket connection and its information.
    """
    host: str
    port: int
    ssl: bool
    connected: bool
    timestamp: float
    ws_connection: Any


@register_output("websocket")
class WebsocketOutput(Output):
    """
    Class which send out filter results via websocket.
    :param host: Host address on which the server will accept incoming connections
    :param port: Server port
    """

    def __init__(self, **kwargs: Any):
        super(WebsocketOutput, self).__init__("websocket", **kwargs)

    def add_parameters(self) -> None:
        self._parser.add_argument("--clients", type=List[Dict[str, Any]], default=[], help="Clients")  # type: ignore
        self._parser.add_argument("--server", type=Dict[str, Any], default={},  # type: ignore
                                  help="Server config: host, port, ssl")
        self._parser.add_argument("--nb-messages-per-second", type=int,
                                  default=30, help="Amount of messages per second")
        self._parser.add_argument(
            "--flow-sent", type=Dict[str, List[str]], default={"stream_types": ["FILTER"], "channel_types": ["OUTPUT"]})  # type: ignore

    def init(self) -> None:

        self._server_is_running: bool = False
        self._stop: bool = False

        # Results stored for each filter, updated every main loop.
        self._filter_results: Dict[str, Any] = {}

        # Setup client connections
        self._connections: List[ClientConnection] = []
        for client in self._args.clients:
            self._connections.append(ClientConnection(
                host=client["host"], port=client["port"], ssl=client["ssl"], connected=False, timestamp=0.0, ws_connection=None))

        if len(self._connections) != 0:
            self._client_thread: threading.Thread = threading.Thread(
                target=self.client_thread_func
            )
            self._client_thread.start()

        # Setup server
        self._serve = bool(self._args.server)
        self._host = self._args.server.get("host", "localhost")
        self._port = self._args.server.get("port", 8765)
        self._ssl_ctx = create_ssl_context(self._args.server.get("ssl", ""))

        if self._serve:
            self._server_thread: threading.Thread = threading.Thread(
                target=self.server_thread_func
            )
            self._server_thread.start()

    def wait_for_server_start(self, timeout: int = 0) -> bool:
        """Blocking function, does not return until the server is running. can
        be run with a timeout, in which case it will return if the server has
        not started before the timeout has expired.

        :param timeout: int - number of seconds after which to return, if the
        server has not started running by then.

        :return: bool - True if server is running. False if timeout reached
        without server started.
        """
        start_time = time.time()
        while not self._server_is_running:
            if timeout > 0 and time.time() > start_time + timeout:
                return False

        return True

    def wait_for_server_stop(self, timeout: int = 0) -> bool:
        """Blocking function, does not return until the server has stopped
        running. can be run with a timeout, in which case it will return if the
        server has not stopped before the timeout has expired.

        :param timeout: int - number of seconds after which to return, if the
        server has not stopped running by then.

        :return: bool - True if server has stopped. False if timeout reached
        without server stopped.
        """
        start_time = time.time()
        while self._server_is_running:
            if timeout > 0 and time.time() > start_time + timeout:
                return False

        return True

    def client_thread_func(self) -> None:
        """
        Function run in separate thread to start asyncio coroutine for
        websocket clients connections. Websocket clients sends out filter
        results continuously on a loop.
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.client_coroutine())
        loop.close()

    async def client_coroutine(self) -> None:
        """
        Asyncio coroutine to run websocket clients inside asynchronous
        context manager.
        """
        while(not self._stop):
            start_time = time.time()
            for connection in self._connections:
                if connection.connected == False:
                    if start_time - connection.timestamp > 10.0:
                        address: str = connection.host
                        port: int = connection.port
                        ssl: bool = connection.ssl
                        try:
                            connection.timestamp = start_time
                            url = f"wss://{address}:{port}" if ssl is True else f"ws://{address}:{port}"
                            ws_connection = await ws.connect(url)  # type: ignore
                            connection.ws_connection = ws_connection

                            logger.info(f"Successfully connected to server at {address}:{port}")
                            connection.connected = True

                        except Exception as e:
                            logger.warning(f"Unable to connect to server at {address}:{port}:{e}")
                else:
                    try:
                        await connection.ws_connection.send(json.dumps(self._filter_results))
                    except:
                        logger.warning("Disconnected from server")
                        connection.connected = False
                        connection.timestamp = time.time()
                        connection.ws_connection = None
            await asyncio.sleep(max(1.0 / self._args.nb_messages_per_second - (time.time()-start_time), 0.0))

    def server_thread_func(self) -> None:
        """
        Function run in separate thread to start asyncio coroutine for
        websocket server. Websocket server sends out filter results
        continuously on a loop.
        """
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.server_coroutine())
        loop.close()
        self._server_is_running = False

    async def server_coroutine(self) -> None:
        """
        Asyncio coroutine to run websocket server inside asynchronous
        context manager.
        """
        async with serve(self.websocket_loop, self._host, self._port,
                         ssl=self._ssl_ctx):
            self._server_is_running = True
            while not self._stop:
                await asyncio.sleep(0.1)

    async def websocket_loop(self, websocket: WebSocketServerProtocol,
                             path: str) -> None:
        """Asyncio coroutine, runs a loop to continually send out filter
        results on websocket server. Exits when the websocket client
        disconnects, or if self.stop() is called.
        """
        logger.info("Websocket client connected!")

        while not self._stop:
            try:
                await websocket.send(json.dumps(self._filter_results))
                await asyncio.sleep(1.0 / self._args.nb_messages_per_second)
            except ConnectionClosed as e:
                # if error code is OK or going away
                if e.code == 1000 or e.code == 1001:
                    break
                else:
                    continue

    def stop(self) -> None:
        self._stop = True

        if len(self._connections) != 0:
            self._client_thread.join()

        if self._serve:
            self._server_thread.join()

    def send_flow(self, flow: Flow, now: float, dt: float) -> None:
        """
        Send the results for the given flow
        :param flow: Flow which outputs will be sent
        """
        streams_dict: Dict[str, Any] = {}
        for stream_type in self._args.flow_sent.get("stream_types"):
            output_streams = flow.get_streams_by_type(Stream.Type[stream_type])
            for name, stream in output_streams.items():
                assert(stream is not None)
                streams_dict[name] = {"type": stream_type, "channels": [], "timestamp": stream.timestamp}
                for channel_type in self._args.flow_sent.get("channel_types"):
                    channels = stream.get_channels_by_type(Channel.Type[channel_type])
                    for channel in channels:
                        output_channel = {"name": channel.name, "type": channel_type,
                                          "data": channel.data, "metadata": channel.metadata}
                        streams_dict[name]["channels"].append(output_channel)

        self._filter_results = {socket.gethostname(): streams_dict}
